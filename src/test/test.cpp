#include <gtest/gtest.h>
#include <Mission.h>
#include <limits.h>
#include <unistd.h>

//Error messages definition. Be careful with the error messages format! 
#define FILE_NOT_FOUND_MSG                 "\033[0;31m[ERROR] Mission specification file has not been found. Parsing cannot continue.\n\033[0m"
// These kind of parsing errors are highly depended of pugi-xml error management. Please feel free to change these messages if the
// message errors or the test files have changed.
#define UNBALANCED_TAGS_MSG_0              "\033[0;31m[ERROR] Start-end tags mismatch: line 21, column 9.\n\033[0m"
#define UNBALANCED_TAGS_MSG_1              "\033[0;31m[ERROR] Start-end tags mismatch: line 22, column 9.\n\033[0m"

#define UNEXPECTED_TAG_MISSION_MSG         "\033[0;31m[ERROR] Unexpected tag \"mision\".The mission declararion is expected to be  <mission name=\"mission_name\">.\n\033[0m"
#define UNEXPECTED_TAG_EVENT_MSG           "\033[0;31m[ERROR] Unexpected tag \"evnt\" for event \"object found\".The declaration of an event is expected to be  <event name=\"event_name\">.\n\033[0m"

#define ACTION_REQUIRED_MSG_0              "\033[0;31m[ERROR] Action required in task \"Stabilizing at origin\".\n\033[0m"

#define UNEXPECTED_ACTION_ATTRIBUTE_MSG_0  "\033[0;31m[ERROR] Unexpected attribute \"nombre\".The declararion of an action is expected to be <action name=\"action_name\"/>.\n\033[0m"   
#define UNEXPECTED_ACTION_ATTRIBUTE_MSG_1  "\033[0;31m[ERROR] Unexpected attribute \"nam\".The declararion of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/>.\n\033[0m" 

#define ACTION_ARGUMENTS_MSG_0             "\033[0;31m[ERROR] Not enough arguments for action \"MOVE\". Argument \"coordinates\" is needed.\n\033[0m"
#define ACTION_ARGUMENTS_MSG_1             "\033[0;31m[ERROR] Too many arguments for action \"TAKE_OFF\" within the task \"Taking off\"\n\033[0m"
#define ACTION_ARGUMENTS_MSG_2             "\033[0;31m[ERROR] Argument passed for action \"WAIT\" must be a number. To specify a decimal value use \".\" instead another symbols.\n\033[0m"
#define ACTION_ARGUMENTS_MSG_3             "\033[0;31m[ERROR] Argument \"(1.2.3, 0, 0)\" passed for action \"MOVE\" must be a point or a variable.\n\033[0m"
#define ACTION_ARGUMENTS_MSG_4             "\033[0;31m[ERROR] Not enough arguments for action \"MOVE\". Argument \"coordinates\" is needed.\n\033[0m"
#define ACTION_ARGUMENTS_MSG_5             "\033[0;31m[ERROR] Incorrect argument attribute \"value\" in action \"MEMORIZE_POINT\". Did you want to say \"label\"?\n\033[0m"
#define ACTION_ARGUMENTS_MSG_6             "\033[0;31m[ERROR] Incorrect argument attribute \"value\" in action \"GO_TO_POINT\". Did you want to say \"label\"?\n\033[0m"


#define UNKNOWN_ACTION_MSG                 "\033[0;31m[ERROR] Unknown action \"STABILICE\".\n\033[0m"

#define CONDITION_SYNTAX_ERROR_MSG_0       "\033[0;31m[ERROR] Syntax error in condition statement within the event \"low battery\". The declaration of a condition is expected to be  <condition parameter=\"parameter_name\" comparison=\"expression\" value=\"condition_value\">.\n\033[0m"   
#define CONDITION_SYNTAX_ERROR_MSG_1       "\033[0;31m[ERROR] Conditional statement expected within the event \"low battery\".\n\033[0m"   

#define ACTION_SYNTAX_ERROR_MSG_0          "\033[0;31m[ERROR] Action statement expected within the event \"low battery\".\n\033[0m"

#define TERMINATION_SYNTAX_ERROR_MSG_0     "\033[0;31m[ERROR] Unexpected tag \"event\" within the event \"low battery\".\n\033[0m"

#define NOT_DECLARED_VARIABLE_MSG_0        "\033[0;31m[ERROR] Variable \"CASA\" in action \"GO_TO_POINT\" has not been declared or it has the name of a reserved word.\n\033[0m"
#define NOT_DECLARED_VARIABLE_MSG_1        "\033[0;31m[ERROR] Variable \"HOME\" in action \"GO_TO_POINT\" has not been declared or it has the name of a reserved word.\n\033[0m"
#define BAD_VARIABLE_NAME_MSG              "\033[0;31m[ERROR] Name \"2HOME\" for point variable in action \"MEMORIZE_POINT\" is not correct. The name of a point variable should not be a reserved word an it should start by a letter followed (optionaly) by alphanumerical characters.\n\033[0m"

using namespace std;

TEST(CompilerTest, MissionWithoutErrors0)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_0.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  //Just checking that no errors have occur.
  EXPECT_TRUE(parser.getErrors().empty());
}

TEST(CompilerTest, MissionWithoutErrors1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_1.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  //Just checking that no errors have occur.
  EXPECT_TRUE(parser.getErrors().empty());
}

TEST(CompilerTest, MissionWithoutErrors2)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_2.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  //Just checking that no errors have occur.
  EXPECT_TRUE(parser.getErrors().empty());
}

TEST(CompilerTest, MissionWithoutErrors3)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_3.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  EXPECT_TRUE(parser.getErrors().empty());
}

TEST(CompilerTest, MissionWithoutErrors4)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_14.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(error_msgs.empty());
}

TEST(CompilerTest, FileNotFoundTest)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("phantom_test.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<std::string> error_msgs = parser.getErrors();
 
  EXPECT_TRUE(!error_msgs.empty());
  EXPECT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), FILE_NOT_FOUND_MSG);
}

TEST(CompilerTest, UnbalancedTagsTest0)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_4.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();

  EXPECT_TRUE(!error_msgs.empty());
  EXPECT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), UNBALANCED_TAGS_MSG_0);
}

TEST(CompilerTest, UnbalancedTagsTest1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_5.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<std::string> error_msgs = parser.getErrors();

  EXPECT_TRUE(!error_msgs.empty());
  EXPECT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), UNBALANCED_TAGS_MSG_1);
}

TEST(CompilerTest, SyntaxErrorsTest0)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_6.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<std::string> error_msgs = parser.getErrors();

  EXPECT_TRUE(!error_msgs.empty());
  EXPECT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), UNEXPECTED_TAG_MISSION_MSG);
}

TEST(CompilerTest, SyntaxErrorsTest1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_7.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();

  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 2);
  EXPECT_EQ(error_msgs.at(0), UNEXPECTED_ACTION_ATTRIBUTE_MSG_0);
  EXPECT_EQ(error_msgs.at(1), ACTION_REQUIRED_MSG_0);
}

TEST(CompilerTest, SyntaxErrorsTest2)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_8.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();

  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 2);
  EXPECT_EQ(error_msgs.at(0), UNEXPECTED_ACTION_ATTRIBUTE_MSG_1);
  EXPECT_EQ(error_msgs.at(1), ACTION_ARGUMENTS_MSG_0);
}

TEST(CompilerTest, SyntaxErrorsTest3)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_9.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 2);
  EXPECT_EQ(error_msgs.at(0), UNEXPECTED_TAG_EVENT_MSG);
  EXPECT_EQ(error_msgs.at(1), CONDITION_SYNTAX_ERROR_MSG_0);
}

TEST(CompilerTest, SyntaxErrorsTest4)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_11.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 3);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_5);
  EXPECT_EQ(error_msgs.at(1), NOT_DECLARED_VARIABLE_MSG_1 );
  EXPECT_EQ(error_msgs.at(2), NOT_DECLARED_VARIABLE_MSG_1 );

}

TEST(CompilerTest, SyntaxErrorsTest5)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_12.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_6);
}

TEST(CompilerTest, SyntaxErrorsTest6)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_13.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), CONDITION_SYNTAX_ERROR_MSG_1);
}

TEST(CompilerTest, SyntaxErrorsTest8)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_15.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  
  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 1);
  EXPECT_EQ(error_msgs.at(0), TERMINATION_SYNTAX_ERROR_MSG_0);
}

TEST(CompilerTest, SemanticErrorsTest0)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_10.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();

  EXPECT_TRUE(!error_msgs.empty());
  ASSERT_TRUE(error_msgs.size() == 7);
  EXPECT_EQ(error_msgs.at(0), ACTION_ARGUMENTS_MSG_1);
  EXPECT_EQ(error_msgs.at(1), UNKNOWN_ACTION_MSG);
  EXPECT_EQ(error_msgs.at(2), BAD_VARIABLE_NAME_MSG);
  EXPECT_EQ(error_msgs.at(3), ACTION_ARGUMENTS_MSG_2);
  EXPECT_EQ(error_msgs.at(4), ACTION_ARGUMENTS_MSG_3);
  EXPECT_EQ(error_msgs.at(5), ACTION_ARGUMENTS_MSG_4);
  EXPECT_EQ(error_msgs.at(6), NOT_DECLARED_VARIABLE_MSG_0);
}

TEST(CompilerTest, SemanticErrorsTest1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_3.xml");
  Parser parser(stack_path + test_path + mission_path);
  TreeNode<Task> tree = parser.generateTaskTree();
  std::vector<Event> events = parser.generateEventVector();
  std::vector<std::string> error_msgs = parser.getErrors();
  EXPECT_TRUE(error_msgs.empty());
}

TEST(MissionTest, TreeTraversalTest1)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_1.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  std::vector<argument<std::string,std::vector<double>>>  arguments;
  Task task;

  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Taking off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Memorize first point");
  EXPECT_EQ(task.getAction()->getReadableName(), "MEMORIZE_POINT");
  // It is mandatory to check if the arguments memorized as labels are stored correctly.
  arguments = task.getAction()->getArguments();
  EXPECT_EQ(arguments.size(), 1);
  EXPECT_EQ(arguments.at(0).value.size(), 1);
  EXPECT_EQ(arguments.at(0).value.at(0), 0);
  task = mission.nextTask();
  // Value should be 0 because the memorized point will be the first one in being stored.
  // If another point is memorized, then its value in the previous vector should be 1.
  EXPECT_EQ(task.getDescription(), "Memorize first point again");
  EXPECT_EQ(task.getAction()->getReadableName(), "MEMORIZE_POINT");
  arguments = task.getAction()->getArguments();
  EXPECT_EQ(arguments.size(), 1);
  EXPECT_EQ(arguments.at(0).value.size(), 1);
  EXPECT_EQ(arguments.at(0).value.at(0), 1);
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Moving to A1");
  EXPECT_EQ(task.getAction()->getReadableName(), "GO_TO_POINT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Frontflip");
  EXPECT_EQ(task.getAction()->getReadableName(), "FLIP");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Landing");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest2)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_2.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  std::vector<argument<std::string,std::vector<double>>>  arguments;
  Task task;
  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Taking off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  //Checking task repetitions
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Moving to A1");
  EXPECT_EQ(task.getAction()->getReadableName(), "MOVE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Frontflip");
  EXPECT_EQ(task.getAction()->getReadableName(), "FLIP");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  //Checking task repetitions
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Moving to A1");
  EXPECT_EQ(task.getAction()->getReadableName(), "MOVE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Frontflip");
  EXPECT_EQ(task.getAction()->getReadableName(), "FLIP");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilizing at origin");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  //Finishing the mission
  EXPECT_EQ(task.getDescription(), "Landing");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest3)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_16.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  Task task;
  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Take Off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "hover");
  EXPECT_EQ(task.getAction()->getReadableName(), "WAIT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "remember coordinates");
  EXPECT_EQ(task.getAction()->getReadableName(), "MEMORIZE_POINT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Move 3");
  EXPECT_EQ(task.getAction()->getReadableName(), "GO_TO_POINT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilize");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Land");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

TEST(MissionTest, TreeTraversalTest4)
{
  std::string stack_path (getenv("AEROSTACK_STACK"));
  std::string test_path ("/stack/planning_system/task_based_mission_planner/task_based_mission_planner/src/test/");
  std::string mission_path ("test_17.xml");
  Parser parser(stack_path + test_path + mission_path);
  std::vector<Event> events = parser.generateEventVector();
  Parameter_Handler parameter_handler;
  Mission mission(parser.generateTaskTree(), parameter_handler);
  Task task;
  
  ASSERT_TRUE(!mission.missionCompleted());
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Take Off");
  EXPECT_EQ(task.getAction()->getReadableName(), "TAKE_OFF");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "hover");
  EXPECT_EQ(task.getAction()->getReadableName(), "WAIT");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Stabilize");
  EXPECT_EQ(task.getAction()->getReadableName(), "STABILIZE");
  task = mission.nextTask();
  EXPECT_EQ(task.getDescription(), "Land");
  EXPECT_EQ(task.getAction()->getReadableName(), "LAND");
  ASSERT_TRUE(mission.missionCompleted());
}

int main (int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS(); 
}