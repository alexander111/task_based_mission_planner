/*!*****************************************************************************
 *  \file      Parser.h
 *  \brief     Definition of all the classes used in the file
 *             Parser.cpp .
 *   
 *  \author    Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef PARSER_H
#define PARSER_H
 
#include <iostream>
#include <cstdlib>
#include <fstream>
#include "pugixml.hpp"

#include "ErrorHandler.h"
#include "Event.h"
#include "TreeNode.h"
#include "Task.h"

class Parser
{
private:
	pugi::xml_document mission_specification_file;
	ErrorHandler parser_error_handler;
  // In order to know what variables can be used, it is necessary to 
  // store each declared variable in the vector below.
  std::vector<std::string> declared_variables;

public:
	//Constructor & Destructor
	Parser(std::string mission_config_file);
	~Parser();
  /*!************************************************************************
   *  \brief  This method generates the task tree where all mission steps
   *          are going to be stored. 
   *  \return Generated task tree.
   *************************************************************************/
  TreeNode<Task> generateTaskTree();

  /*!************************************************************************
   *  \brief  This method generates the event vector where all mission events
   *          are going to be stored. 
   *  \return Generated event vector.
   *************************************************************************/
  std::vector<Event> generateEventVector(); 
  /*!************************************************************************
   *  \brief  This function returns a vector in which the detected errors
   *          have been stored. 
   *  \return A vector of string error messages.
   *************************************************************************/
  std::vector<std::string> getErrors();
  /*!************************************************************************
   *  \brief  This function displays the errors found during the parsing
   *          process. 
   *************************************************************************/  
  void displayFoundErrors(); 

private:
	//Auxiliar methods
  /*!************************************************************************
   *  \brief  This method read and set a new task in the mission's tree.
   *  \param  taskRead Task's data obtained from the xml file.
   *  \param  taskInTree Pointer to a certain task of the mission's tree.
   *  \param  name The absolute name of the task for which the parameters are
   *          going to be read and set.
   *************************************************************************/
    void readAndSetTaskParameters(const pugi::xml_node nodeRead, Task *taskInTree, std::string name);
   /*!************************************************************************
   *  \brief  This method translates a read value into an action command. 
   *  \param  readingValue String to be recognized or translated.
   *  \return An action command is returned.
   *************************************************************************/
    ActionType recognizeAction(std::string readingValue);
  /*!************************************************************************
   *  \brief  This method translates a read value into a skill command. 
   *  \param  readingValue String to be recognized or translated.
   *  \return A skill command is returned.
   *************************************************************************/
    SkillType recognizeSkill(std::string readingValue);
  /*!************************************************************************
   *  \brief  This function checks if the given parameter is correct. If an
   *          error is found it is printed by the error handler.
   *  \param  parameter Parameter to be recognized.
   *************************************************************************/
    void recognizeParameter(std::string parameter);
      /*!************************************************************************
   *  \brief  This function checks if the given comparison is correct. If an
   *          error is found it is printed by the error handler.
   *  \param  parameter Parameter to be recognized.
   *************************************************************************/
    void recognizeComparison(std::string comparison);
  /*!************************************************************************
   *  \brief  This method translates a read value into an action command. 
   *  \param  readingValue String to be recognized or translated.
   *  \return An action command is returned.
   *************************************************************************/
  EndingStepType recognizeEndingStep(std::string readingValue);
  /*!************************************************************************
   *  \brief  This auxiliar method helps function generateTaskTree().
   *  \param  curent_task Current task's data obtained from the xml file.
   *  \param  tree TreeNode object in which current_task information is 
   *          going to be stored.
   *  \param  name Current task's absolute name.
   *  \param  is_root_task Auxiliar paramiter.
   *  \return A TreeNode object in which the information read from the xml
   *          file has been stored.
   *  \see    generateTaskTree()
   *************************************************************************/
    TreeNode<Task> generateTaskTreeAux(pugi::xml_node current_task, TreeNode<Task> tree, std::string name, bool is_root); 
};
#endif