/*!*****************************************************************************
 *  \file    Task.h
 *  \brief   Definition of all the classes used in the file
 *           Task.cpp .
 *   
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef TASK_H
#define TASK_H

#include <iostream>
#include <cstdlib>
#include <vector>
#include "Action.h"
#include "Condition.h"

/*!***************************************************************************
 *  \class Task
 *  \brief This class represents an abstraction of a Task.
 *****************************************************************************/
class Task
{
private:
  std::string absolute_name;
  std::string description;
  Action action; //A task must have always one action. If the action is not specified, then the action value is empty.
  std::vector<SkillType> skills;//A task may have several skills activated.
  std::vector<Condition> conditions;
  int times_to_repeat;

public:
  /*!************************************************************************
   *  \brief  This method sets the task's description from a given value. 
   *  \param  task_description Task's description.
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/  
  int setDescription(std::string task_description);
  /*!************************************************************************
   *  \brief  This method sets the task's action from a given value. 
   *  \param  action Task's action.
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/ 
  int setAction(Action action);
    /*!************************************************************************
   *  \brief  This method sets the task's absolute name from a given parameter. 
   *  \param  name Task's absolute name in the task tree of the mission.
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/ 
  int setAbsoluteName(std::string name);
 /*!************************************************************************
   *  \brief  This method sets the task's skills from a given value. 
   *  \param  task_skills Task's skills.
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/ 
  int setSkills(std::vector<SkillType> task_skills);
  /*!************************************************************************
   *  \brief  This method sets the task's conditions from a given value. 
   *  \param  task_conditions Task's conditions.
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/ 
  int setConditions(std::vector<Condition> task_conditions);
  /*!************************************************************************
   *  \brief  This method sets the task's reptitions from a given value. 
   *  \param  n Number of repetitions.
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/ 
  int setTimesToRepeat(int n);
  /*!************************************************************************
   *  \brief  This method gets the task's description. 
   *  \return The task's description.
   *************************************************************************/ 
  std::string getDescription();
  /*!************************************************************************
   *  \brief  This method gets the task's action. 
   *  \return The task's action.
   *************************************************************************/ 
  Action * getAction();
  /*!************************************************************************
   *  \brief  This method gets the task's skills. 
   *  \return The task's description.
   *************************************************************************/ 
  std::vector<SkillType> * getSkills();
  /*!************************************************************************
   *  \brief  This method gets the task's conditions. 
   *  \return A pointer to a vector of task's conditions.
   *************************************************************************/ 
  std::vector <Condition> * getConditions();
    /*!************************************************************************
   *  \brief  This method returns the absolute name of the task within the 
   *          mission's task tree. 
   *  \return A string with the absolute name.
   *************************************************************************/ 
  std::string getAbsoluteName();
  /*!************************************************************************
   *  \brief  This method gets the number of repetitions. 
   *  \return The number of repetitions.
   *************************************************************************/ 
  int getTimesToRepeat();
  /*!************************************************************************
   *  \brief  This method checks if the task can be executed according to 
   *          the task conditions.
   *  \param  parameter_handler  Were the status of the parameters are stored
   *  \return True if the task can be executed, false in other case.
   *************************************************************************/ 
  bool checkTaskConditions(Parameter_Handler parameter_handler);

public:
  Task();  // Class constructor
  ~Task();   // Class destructor
};
#endif