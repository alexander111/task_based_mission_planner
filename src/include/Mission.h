/*!*****************************************************************************
 *  \file      Mission.h
 *  \brief     Definition of all the classes used in the file
 *             Mission.cpp .
 *   
 *  \author    Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef MISSION_H
#define MISSION_H
#include <iostream>
#include <cstdlib>
#include "Parser.h"
#include "Parameter_Handler.h"
/*!***************************************************************************
 *  \class Mission
 *  \brief This class represents an abstraction of what a mission 
 *         is supossed to be.
 *****************************************************************************/
class Mission
{
private:
    std::string name;
    Task current_mission_task;
    TreeNode<Task> taskTree;
    Parameter_Handler *parameter_handler;

public:
    //Constructor & Destructor
    Mission(TreeNode<Task> generatedTaskTree, Parameter_Handler &par_handler);
    ~Mission();

public:
  /*!************************************************************************
   *  \brief  This method prints the task tree where all mission steps
   *          are stored. 
   *  \return 0 if finished without errors, !=0 in other case.
   *************************************************************************/
    int printTree();
  /*!************************************************************************
   *  \brief  This method checks if the main mission has been completed.
   *  \return True if the mission has been completed, false in other case.
   *************************************************************************/
    bool missionCompleted();
  /*!************************************************************************
   *  \brief  This method returns the next task of the mission 
   *  \return An object of the class Task which is the next task 
   *          to be performed.
   *************************************************************************/
    Task nextTask();
  /*!************************************************************************
   *  \brief  This method gets the current active skills of the mission.
   *  \return A vector of all the skills that are currently activated.
   *************************************************************************/
    std::vector<SkillType> getActiveSkills();
  /*!************************************************************************
   *  \brief  This method gets the name of the current mission.
   *  \return A string with the name of the current mission.
   *************************************************************************/
    std::string getName();

private:  
  /*!************************************************************************
   *  \brief  This auxiliar method helps methods nextTask().
   *  \param  tree A TreeNode object in which search the next task to send.
   *  \return A task object.
   *  \see    nextTask().
   *************************************************************************/
    Task nextTaskAux(TreeNode<Task> &tree);
  /*!************************************************************************
   *  \brief  This auxiliar method helps function nextTask().
   *  \param  tree A TreeNode object in which search the next task to send.
   *  \return A task object.
   *  \see    nextTask().
   *************************************************************************/
    std::string printTreeAux(TreeNode<Task> tree, std::string message, std::string acc);
      /*!************************************************************************
   *  \brief  This auxiliar method helps function getActiveSkills().
   *  \param  tree TreeNode object where the mission is stored.
   *  \return A vector of active skills.
   *  \see    getActiveSkills()
   *************************************************************************/
    std::vector<SkillType> getActiveSkillsAux(TreeNode<Task> tree);   
};
#endif
