/*!*****************************************************************************
 *  \file      definitions.h
 *  \brief     Allowed actions are defined in this file.  
 *  \author    Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef DEFINITIONS_H
#define DEFINITIONS_H
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>

//Action Commands
#define CMD_TAKEOFF                         "TAKE_OFF"
#define CMD_HOVER                           "WAIT"
#define CMD_LAND                            "LAND"
#define CMD_STABILIZE                       "STABILIZE"
#define CMD_FLIP                            "FLIP"
 
#define CMD_MOVEMENT_POSITION               "MOVE"
#define CMD_GO_TO_POINT                     "GO_TO_POINT"
#define CMD_ROTATE_YAW                      "ROTATE_YAW"
#define CMD_REMEMBER_POINT                  "MEMORIZE_POINT"

//Action arguments name value
#define NAME_DIRECTION                      "direction"
#define NAME_DESTINATION                    "coordinates"
#define NAME_DURATION                       "duration"
#define NAME_ROTATION                       "orientation angle" 

//Argument attributes'name
 #define ATTR_NAME                          "name"
 #define ATTR_VALUE                         "value"
 #define ATTR_LABEL                         "label"

//Skill commands
#define BHV_AVOIDING_OBSTACLES              "AVOID_OBSTACLES"
#define BHV_RECOGNIZING_ARUCO_MARKERS       "RECOGNIZE_ARUCO_MARKERS"
#define BHV_RECOGNIZING_VISUAL_MARKERS      "RECOGNIZE_VISUAL_MARKERS"
#define BHV_SAYING_OUT_LOUD_CURRENT_TASK    "SAY_OUT_LOUD_CURRENT_TASK"

//Ending Actions
#define END_REPEAT_TASK                     "REPEAT_TASK"
#define END_NEXT_TASK                       "NEXT_TASK"
#define END_ABORT_MISSION                   "ABORT_MISSION"
#define END_END_MISSION                     "END_MISSION"

//Condition comparison symbols
#define COMP_EQUAL                          "equal"
#define COMP_INCLUDES                       "includes"
#define COMP_NOT_EQUAL                      "not equal to"
#define COMP_LESS                           "less than"
#define COMP_LESS_OR_EQUAL                  "less than or equal to"
#define COMP_GREATER                        "greater than"
#define COMP_GREATER_OR_EQUAL               "greater than or equal to"

//Condition Parameters
#define PAR_ARUCO_VISUAL_MARKER             "RECOGNIZED_ARUCO_MARKERS"
#define PAR_BATTERY_CHARGE_PERCENTAGE       "BATTERY_CHARGE_PERCENTAGE"
#define PAR_CURRENT_TASK                    "CURRENT_TASK"
 
//Action types
enum class ActionType
{
  takeOff,       
  hover,      
  land,
  stabilize,
  positionMovement,
  goToPoint,
  rotateYaw,
  flipMovement,    
  flipMovementRight,    
  flipMovementLeft,    
  flipMovementFront,   
  flipMovementBack,
  rememberPoint,
  empty
};

//Skill types.
enum class SkillType
{
  avoidingObstacles,
  recognizingArucoMarkers,
  recognizingVisualMarkers,
  sayingOutLoudCurrentTask
};

//Termination mode types (Control actions, also called ending actions). This set of actions is different from the previous one.
enum EndingStepType
{
  repeatTask,
  nextTask,
  abortMission,
  endMission
};

/*!************************************************************************
 *  \struct point
 *  \brief  An abstraction of a tridimensional point.
 *************************************************************************/
struct point 
{
  //If z = 0 is set, then it will be considered as a bidimensional point. (Z=0 plane)
  float x; //!< The coordinate x
  float y; //!< The coordinate y
  float z; //!< The coordinate z
};


/*!************************************************************************
 *  \struct argument
 *  \brief  An abstract element of a custom action argument data structure.
 *************************************************************************/
template <typename A, typename B>
struct argument
{
  A name;
  B value;
  std::string name_string;
  std::string value_string;
};
#endif