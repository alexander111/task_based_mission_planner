/*!*****************************************************************************
 *  \file    Condition.h
 *  \brief   Definition of all the classes used in the file
 *           Condition.cpp .
 *   
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef CONDITION_H
#define CONDITION_H
#include "Definitions.h"
#include "Parameter_Handler.h"
#include <iostream>
#include <cstdlib>

/*!***************************************************************************
 *  \class Condition
 *  \brief This class represents a definition of what a condition is.
 *****************************************************************************/
class Condition{
private:
bool condition_evaluation;
std::string parameter;
std::string comparison;
std::string value;
public:
  //Constructor & Destructor
  Condition(std::string condition_parameter, std::string condition_comparison, std::string condition_value);
  ~Condition();

public:
 /*!************************************************************************
  *  \brief  This method evaluates the condition according to the parameter
  *          that is passed.
  *  \param  condition_parameter Parameter needed to evaluate 
  *          the whole condition.
  *  \return The boolean evaluation of the condition.
  *************************************************************************/
  bool evaluateCondition(Parameter_Handler parameter_handler);
 /*!************************************************************************
  *  \brief  This method checks if the param,eter passed as argument is equal
  *          to the parameter of the condition.
  *  \param  condition_parameter Parameter needed to evaluate 
  *          the whole condition.
  *  \return True if the argument passed is equal to the condition's 
  *          parameter.
  *************************************************************************/
  bool isParameterEqualTo(std::string condition_parameter);
 /*!************************************************************************
  *  \brief  This method returns the value of the condition.
  *  \return The value of the condition.
  *************************************************************************/
  bool getConditionValue();
 /*!************************************************************************
  *  \brief  Reset the condition's attributes to its default values.
  *************************************************************************/
  void resetConditionEvaluation();
 /*!************************************************************************
  *  \brief  This method returns the expression of the condition.
  *  \return A trsing with the proper expression.
  *************************************************************************/  
  std::string getConditionExpresion();

private:
/*!************************************************************************
  *  \brief  This method translates from string to a integer vector. The 
  *          expected syntax for the string message is the following: 
  *          [0,1,...].
  *  \param  vector The string to translate.
  *  \return A vector of integers.
  *************************************************************************/
  std::vector<int> fromStringtoIntVector(std::string vector);
/*!************************************************************************
  *  \brief  This method checks if an element "n" belongs to the vector 
  *          given as argument.
  *  \param  n An integer.
  *  \param  vec A vector of integers
  *  \return True if the element "n" belongs to the vector "vec", false in 
  *          other case.
  *************************************************************************/
  bool belongsInt( int n, std::vector<int> vec);
/*!************************************************************************
  *  \brief  This method cheks if the string given as argument is an 
  *          integer.
  *  \param  s The string to check.
  *  \return True if the string is an integer, false in other case.
  *************************************************************************/
  bool is_number(const std::string& s);
};
#endif