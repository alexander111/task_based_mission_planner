/*!*****************************************************************************
 *  \file      Event.h
 *  \brief     Definition of all the classes used in the file
 *             Event.cpp .
 *   
 *  \author    Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef EVENT_H
#define EVENT_H
 
#include <iostream>
#include <cstdlib>
#include <vector>
#include "Condition.h"
#include "Action.h"
#include "Parameter_Handler.h"

/*!***************************************************************************
 *  \class Event
 *  \brief This class represents an abstraction of what a Event 
 *         is supossed to have and represent.
 *****************************************************************************/
class Event{
private:
    std::vector<Condition> conditions;
    std::vector<Action> actions;
    std::vector<Action> remaining_actions;
    std::string description;
    EndingStepType ending_step;

public:
    //Constructor & Destructor
    Event(std::string eventDescription, std::vector<Condition> eventConditions, std::vector<Action> eventActions, EndingStepType event_ending_step);
    ~Event();
  /*!************************************************************************
   *  \brief  Checks if the event has been activated.
   *  \return True if the event has been activated, False in other case.
   *************************************************************************/
    bool isEventActivated(Parameter_Handler par);
  /*!************************************************************************
   *  \brief  Event's description is returned.
   *  \return A String with the event's description
   *************************************************************************/
    std::string getDescription();
  /*!************************************************************************
   *  \brief  Event's actions.
   *  \return Pointer to a vector of actions.
   *************************************************************************/
    std::vector<Action> * getActions();
  /*!************************************************************************
   *  \brief  Remaining actions to perform.
   *  \return Pointer to a vector of actions.
   *************************************************************************/
    std::vector<Action> * getRemainingActions();
  /*!************************************************************************
   *  \brief  Final step to perform is returned.
   *  \return A singular ending action.
   *************************************************************************/
    EndingStepType getEndingStep();
  /*!************************************************************************
   *  \brief  Conditions that must be true in order to start an event are
   *          returned. 
   *  \return Pointer to a vector of conditions.
   *************************************************************************/
    std::vector<Condition> * getConditions();

    void resetEvent();
};
#endif
