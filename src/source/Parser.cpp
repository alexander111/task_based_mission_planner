#include <stdio.h>
#include <iostream>
#include <cmath>
#include "Parser.h"

using namespace std;

Parser::Parser(std::string mission_config_file)
{
  //Reading and loading file. 
  std::ifstream nameFile(mission_config_file.c_str());
  pugi::xml_parse_result result = mission_specification_file.load(nameFile);
  //Parsing errors
  parser_error_handler.checkParsingErrors(result, mission_config_file);
}

Parser::~Parser()
{

}

TreeNode<Task> Parser::generateTaskTree(){
  //Data structure where the task tree is going to be stored.
  TreeNode<Task> taskTree;
  std::string name;
  pugi::xml_node current_task = mission_specification_file.first_child();
  //The first tag expected is the tag "mission".
  if(string(current_task.name())!= "mission")
    parser_error_handler.errorFound("Unexpected tag \"" + string(current_task.name()) + "\".The mission declararion is expected to be  <mission name=\"mission_name\">." );
  if(string(current_task.first_attribute().name()) != "name")
    parser_error_handler.errorFound("Unexpected attribute \"" + string(current_task.first_attribute().name()) + "\".The mission declararion is expected to be <mission name=\"mission_name\">." );
  if(current_task.first_attribute().next_attribute())
    parser_error_handler.errorFound("Too many attributes detected for mission tag. The mission declararion is expected to be <mission name=\"mission_name\">." );

  taskTree = generateTaskTreeAux(current_task, taskTree, name, true);
  return taskTree;
}

TreeNode<Task> Parser::generateTaskTreeAux(pugi::xml_node current_task, TreeNode<Task> tree, std::string name, bool is_root)
{
  //Variable declaration.
  Task task_tree;
  std::vector<TreeNode<Task>> children;
  std::vector<TreeNode<Task>> children_repetitions;
  int times_to_repeat;
  bool is_root_task = is_root;
  //Saving absolute name for each task.
  if(string(current_task.name()) == "mission")
  {
    name="";
    is_root_task = true;
  }
  else if(is_root_task)
  {
    name = name + string(current_task.first_attribute().value());
    is_root_task = false;
  }
  else 
    name = name + "/" + string(current_task.first_attribute().value());
  // For every child, a new sub-tree is generated recursively.
  for(pugi::xml_node child = current_task.first_child(); child; child = child.next_sibling())
  {
    if(string(child.name()) == "task")
    {
      TreeNode<Task> child_tree;
      child_tree = generateTaskTreeAux(child, child_tree, name, is_root_task);
      children.push_back(child_tree);
      //Tags different from "task and "event_handling are not allowed by definition"
      if(child.next_sibling() && string(child.next_sibling().name()) != "task" && string(child.next_sibling().name()) != "event_handling")
          parser_error_handler.errorFound("Unexpected tag \"" + string(child.next_sibling().name()) + "\".The task declararion is expected to be <task name=\"task_name\">." );
    }
  }
  //Finally, task parameters have to be read  and set in the tree.
  readAndSetTaskParameters(current_task, tree.getNode(), name);
  //The tree must be modified according to the number of repetitions specified for each child task.
  for(uint i=0; i < children.size(); i++)
  {
    times_to_repeat = children.at(i).getNode()->getTimesToRepeat();
    for(uint j=0; j<times_to_repeat; j++)
      children_repetitions.push_back(children.at(i));
  }
  tree.setChildren(children_repetitions);
  //Checking syntax errors.
  if(tree.getChildren()->empty() && tree.getNode()->getAction()->isEmpty())
    parser_error_handler.errorFound("Action required in task \"" + tree.getNode()->getDescription() + "\".");
  return tree;
}

void Parser::readAndSetTaskParameters(const pugi::xml_node taskRead, Task *taskInTree, std::string name){
  //Read parameters
  std::string readingValue;
  //Vector where the skills of the task are going to be stored
  std::vector<SkillType> task_skills;
  //Variable in which the messages refering to semantic errors are going to be stored.
  std::string semantic_errors;
  //Action that is going to be read.
  Action action;
  action.setActionType(ActionType::empty);
  //Action argument data structure.
  argument <std::string, std::string> action_argument;
  //Description reading.
  std::string task_description;
  // Task conditions
  std::vector<Condition> task_conditions;
  // Times to repeat
  int times_to_repeat;

  if(string(taskRead.first_attribute().name()) == "name")
    task_description  = taskRead.first_attribute().value();
  else
    parser_error_handler.errorFound("Unexpected attribute \"" + string(taskRead.first_attribute().name()) + "\". The declaration of a task is expected to be <task name=\"task_name\"/>." );

  for(pugi::xml_node child = taskRead.first_child(); child && string(child.name())!= "task" && string(child.name())!= "event_handler"; child = child.next_sibling())
  {
    //Reading conditions if any.
    while(child && string(child.name())=="condition")
    {
      if(string(child.first_attribute().name()) == "parameter" && string(child.first_attribute().next_attribute().name()) == "comparison" && string(child.first_attribute().next_attribute().next_attribute().name()) == "value" && !child.first_attribute().next_attribute().next_attribute().next_attribute())
      {
        recognizeParameter(child.attribute("parameter").value());
        recognizeComparison(child.attribute("comparison").value());
        Condition condition(child.attribute("parameter").value(), child.attribute("comparison").value(), child.attribute("value").value());
        task_conditions.push_back(condition);
      }
      else if(string(child.first_attribute().name()) == "currentTask" && !child.first_attribute().next_attribute())
      {
        Condition condition(PAR_CURRENT_TASK, COMP_EQUAL, child.first_attribute().value());
        task_conditions.push_back(condition);
      }
      else 
        parser_error_handler.errorFound("Syntax error in condition statement within the task \"" + task_description + "\". The declaration of a condition is expected to be  <condition parameter=\"parameter_name\" comparison=\"expression\" value=\"condition_value\">." );             
      child = child.next_sibling();   
    } 
    if(string(child.name())=="repeat")
    {
      if(string(child.first_attribute().name()) == "times")
      {
        if(std::floor(stod(child.first_attribute().value())) != stoi(child.first_attribute().value()))
          parser_error_handler.warningFound("Decimal value found in the repeat statement within task\"" + task_description + "\", it will be rounded to the closest integer.");
        if(stoi(child.first_attribute().value())<=0)
          parser_error_handler.errorFound("The number of times to repeat must be a positive integer, found \"" + string(child.first_attribute().value()) + "times within task \"" + task_description + "\".");
        if(child.first_attribute().next_attribute())
          parser_error_handler.errorFound("Unexpected attribute \"" + string(child.first_attribute().next_attribute().name()) + "\". The declaration of a repeat statement is expected to be <repeat times=\"number\"/>." );
        else
        {
          times_to_repeat = stoi(child.first_attribute().value());    
          taskInTree->setTimesToRepeat(times_to_repeat);
        }    
      }
      else
        parser_error_handler.errorFound("Unexpected attribute \"" + string(child.first_attribute().name()) + "\".The declararion of a repeat statement is expected to be <repeat times=\"number\"/>.");
      child = child.next_sibling();
    }   
    //Reading skills if any.
    while(child && string(child.name())=="skill")
    {
      if(string(child.first_attribute().name()) == "name")
        task_skills.push_back(recognizeSkill(child.first_attribute().value()));       
      else
        parser_error_handler.errorFound("Unexpected attribute \"" + string(child.first_attribute().name()) + "\".The declararion of a skill is expected to be <skill name=\"skill_name\"/>." );     
      if(child.first_attribute().next_attribute())
        parser_error_handler.errorFound("Too many attributes detected for skill tag. The declaration of a skill is expected to be <skill name=\"skill_name\"/>." );
      child = child.next_sibling();
    }

    //The action of the current task has to be read and recognized if any.
    //By definition, a mother task (an intern node in the tree) cannot have an action defined.  
    if(taskRead.child("task") && string(child.name())=="action")
        parser_error_handler.errorFound( "A mother task cannot have an action within. Task with description \""+ task_description + "\" has the ilegal action \"" + readingValue + "\".");
    else if( taskRead.child("task") && string(child.name())!="task")
      parser_error_handler.errorFound("Expected task statement within task \"" + task_description + "\" but found \"" + string(child.name()) + "\".");
    else if(!taskRead.child("task") && string(child.name())=="action")
    {
      if(string(child.first_attribute().name()) == "name" && action.isEmpty())
      {
        action.setActionType(recognizeAction(child.first_attribute().value()));
        //Once the action has been recognized, the arguments must be found as well.
        for(pugi::xml_node argument = child.first_child(); argument; argument = argument.next_sibling())
        {
          if(string(argument.name())=="argument")
          {
            if(string(argument.first_attribute().name()) == ATTR_NAME)
            {
              if(string(argument.first_attribute().next_attribute().name()) == ATTR_VALUE || string(argument.first_attribute().next_attribute().name()) == ATTR_LABEL)
              {
                //Setting up the arguments
                action_argument.name   = argument.first_attribute().value();
                action_argument.value = argument.first_attribute().next_attribute().value();
                action_argument.name_string = string(argument.first_attribute().name());
                action_argument.value_string = string(argument.first_attribute().next_attribute().name());
                //If the argument cannot be set up, then it means that the number of arguments is incorrect.
                if(!action.setArgument(action_argument))
                  parser_error_handler.errorFound("Too many arguments for action \"" + action.getReadableName() + "\" within the task \"" + task_description +"\"" );
                //If there are more attributes, an error has been found.
                if(argument.first_attribute().next_attribute().next_attribute())
                  parser_error_handler.errorFound("Too many attributes detected for argument tag. The declaration of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/>. " );              
              }
              else
                parser_error_handler.errorFound("Unexpected attribute \"" + string(argument.first_attribute().next_attribute().name()) + "\".The declararion of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/>." );
            }
            else
              parser_error_handler.errorFound("Unexpected attribute \"" + string(argument.first_attribute().name()) + "\".The declararion of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/>." );     
          }
          else 
            parser_error_handler.errorFound("Unexpected tag \"" + string(argument.name()) + "\" within the task \"" + task_description + "\". The declararion of an argument is expected to be  <argument name=\"action_name\" value=\"argument_value\">." );
        }
        //Finally it is mandatory to check the semantic properties of the read action
        if(!action.checkActionSemanticErrors(semantic_errors, declared_variables))
          parser_error_handler.errorFound(semantic_errors);
      }
      else if(!action.isEmpty())
        parser_error_handler.errorFound("Unexpected action \"" + string(child.first_attribute().value()) + "\" within the task \"" + task_description + "\". A task can only have one action." );     
      else
        parser_error_handler.errorFound("Unexpected attribute \"" + string(child.first_attribute().name()) + "\".The declararion of an action is expected to be <action name=\"action_name\"/>." );     
      if(child.first_attribute().next_attribute())
         parser_error_handler.errorFound("Too many attributes detected for skill tag. The declaration of a skill is expected to be <skill name=\"skill_name\">." );
    }
    else if(!taskRead.child("task") && string(child.name())!="action")
      parser_error_handler.errorFound("Unexpected tag \"" + string(child.name()) + "\" within the task \"" + task_description + "\". The declararion of an action is expected to be  <action name=\"action_name\">." );
    //Else just continue the execution of the program
    else;
  }
  
  //Setting task attributes.
  taskInTree->setAbsoluteName(name);
  taskInTree->setDescription(task_description);
  taskInTree->setAction(action);
  taskInTree->setSkills(task_skills);
  taskInTree->setConditions(task_conditions);
}

std::vector<Event> Parser::generateEventVector(){

  pugi::xml_node event_handling_node = mission_specification_file.first_child();

  argument<std::string,std::string> action_argument;
  std::vector<Event> event_vector;
  std::vector<Action> event_actions;
  std::string event_description;
  std::string event_ending_step;
  std::string semantic_errors;
  std::vector<Condition> event_conditions;
  
  if(string(event_handling_node.name())!= "mission")
    parser_error_handler.errorFound("Unexpected tag \"" + string(event_handling_node.name()) + "\".The mission declararion is expected to be  <mission name=\"mission_name\">  " );
  if(string(event_handling_node.first_attribute().name()) != "name")
    parser_error_handler.errorFound("Unexpected attribute \"" + string(event_handling_node.first_attribute().name()) + "\".The mission declararion is expected to be <mission name=\"mission_name\">. " );
  if(event_handling_node.first_attribute().next_attribute())
    parser_error_handler.errorFound("Too many attributes detected for mission tag. The mission declararion is expected to be <mission name=\"mission_name\">. " );

  
  for(event_handling_node = event_handling_node.first_child(); event_handling_node; event_handling_node = event_handling_node.next_sibling())
  {   
    if(string(event_handling_node.name()) != "task" && string(event_handling_node.name()) != "event_handling")
      parser_error_handler.errorFound("Unexpected tag \"" + string(event_handling_node.name()) + "\".The task declararion is expected to be  <task name=\"task_name\">. " );
    else if( string(event_handling_node.name()) == "event_handling")
    {
      for(pugi::xml_node event = event_handling_node.first_child(); event; event = event.next_sibling())
      {
        //Event Description.
        event_description = event.first_attribute().value();
        if(string(event.name())== "event")
        {
          if(string(event.first_attribute().name())=="name")
          { 
            if(event.first_attribute().next_attribute())
              parser_error_handler.errorFound("Too many attributes detected for event tag. The declaration of an event is expected to be <event name=\"event_name\">. " );

            //This variable will be the one used to parser all the body of the current event, from conditions to termination mode.
            pugi::xml_node child = event.first_child();
            //Reading conditions. An event must have at leats one condition.
            do
            {
              if(string(child.name())=="condition")
              {
                if(string(child.first_attribute().name()) == "parameter" && string(child.first_attribute().next_attribute().name()) == "comparison" && string(child.first_attribute().next_attribute().next_attribute().name()) == "value" && !child.first_attribute().next_attribute().next_attribute().next_attribute())
                {
                  recognizeParameter(child.attribute("parameter").value());
                  recognizeComparison(child.attribute("comparison").value());
                  Condition condition(child.attribute("parameter").value(), child.attribute("comparison").value(), child.attribute("value").value());
                  event_conditions.push_back(condition);
                }
                else if(string(child.first_attribute().name()) == "currentTask" && !child.first_attribute().next_attribute())
                {
                  Condition condition(PAR_CURRENT_TASK, COMP_EQUAL, child.first_attribute().value());
                  event_conditions.push_back(condition);
                }
                else parser_error_handler.errorFound("Syntax error in condition statement within the event \"" + event_description + "\". The declaration of a condition is expected to be  <condition parameter=\"parameter_name\" comparison=\"expression\" value=\"condition_value\">." );             
                child = child.next_sibling();
              }
              else 
                parser_error_handler.errorFound("Conditional statement expected within the event \"" + event_description + "\".");
            }
            while(child && string(child.name())=="condition");
          
            //Reading actions of the event. An event may have 0 actions according to Aerostack's wiki mission specification chapter.
            while(child && string(child.name())=="action") 
            {
                Action event_action;
                event_action.setActionType(ActionType::empty);                    
                if(string(child.first_attribute().name()) == "name" )
                {
                  event_action.setActionType(recognizeAction(child.first_attribute().value()));
                  //Once the action has been recognized, the arguments must be found as well.
                  for(pugi::xml_node argument = child.first_child(); argument; argument = argument.next_sibling())
                  {
                    if(string(argument.name())=="argument")
                    {
                      if(string(argument.first_attribute().name()) == ATTR_NAME)
                      {
                        if(string(argument.first_attribute().next_attribute().name()) == ATTR_VALUE|| string(argument.first_attribute().next_attribute().name()) == ATTR_LABEL)
                        {   
                          //Setting up the arguments
                          action_argument.name   = argument.first_attribute().value();
                          action_argument.value = argument.first_attribute().next_attribute().value();
                          action_argument.name_string = string(argument.first_attribute().name());
                          action_argument.value_string = string(argument.first_attribute().next_attribute().name());
                          //If the argument cannot be set up, then it means that the number of arguments is incorrect.
                          if(!event_action.setArgument(action_argument))
                            parser_error_handler.errorFound("Too many arguments for action \"" + event_action.getReadableName() + "\" within the event \"" + event_description + "\"." );
                          //If there are more attributes, an error has been found.
                          if(argument.first_attribute().next_attribute().next_attribute())
                            parser_error_handler.errorFound("Too many attributes detected for argument tag. The declaration of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/>." );              
                        }
                        else
                          parser_error_handler.errorFound("Unexpected attribute \"" + string(argument.first_attribute().next_attribute().name()) + "\".The declararion of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/> or <argument name=\"argument_name\" label=\"label_name\"/>." );
                      }
                      else
                        parser_error_handler.errorFound("Unexpected attribute \"" + string(argument.first_attribute().name()) + "\".The declararion of an argument is expected to be <argument name=\"argument_name\" value=\"argument_value\"/>." );     
                    }
                    else 
                      parser_error_handler.errorFound("Unexpected tag \"" + string(argument.name()) + "\" within the event \"" + event_description + "\". The declararion of an argument is expected to be  <argument name=\"argument_name\" value=\"argument_value\">." );
                  }
                  //Finally it is mandatory to check the semantic properties of the read action
                  if(!event_action.checkActionSemanticErrors(semantic_errors, declared_variables))
                    parser_error_handler.errorFound(semantic_errors);

                  event_actions.push_back(event_action);
                }
                else if(!event_action.isEmpty())
                  parser_error_handler.errorFound("Unexpected action \"" + string(child.first_attribute().value()) + "\" within the event \"" + event_description + "\" .A task can only have one action." );     
                else
                  parser_error_handler.errorFound("Unexpected attribute \"" + string(child.first_attribute().name()) + "\".The declararion of an action is expected to be <action name=\"action_name\"/>." );     
                if(child.first_attribute().next_attribute())
                  parser_error_handler.errorFound("Too many attributes detected for skill tag. The declaration of a skill is expected to be <skill name=\"skill_name\">." );
              //Continue looping.
              child = child.next_sibling();
            }
            //A termination mode is mandatory
            if(string(child.name())=="termination")
            {
              if(string(child.first_attribute().name())=="mode")
              {
                event_ending_step = event.child("termination").attribute("mode").value();
                Event event_read(event_description, event_conditions, event_actions, recognizeEndingStep(event_ending_step));
                event_vector.push_back(event_read);
                event_conditions.clear();
                event_actions.clear();
              }
              else
                parser_error_handler.errorFound("Unexpected attribute \"" + string(child.first_attribute().name()) + "\".The declaration of a termination mode is expected to be <termination mode=\"termination_mode_name\">." );
            }
            else
              parser_error_handler.errorFound("Unexpected tag \"" + string(event.name()) + "\" within the event \"" + event_description + "\".");    
            //Continue looping.
            child = child.next_sibling();
          }
        }
        else
          parser_error_handler.errorFound("Unexpected tag \"" + string(event.name()) + "\" for event \"" + event_description + "\".The declaration of an event is expected to be  <event name=\"event_name\">." );
      }
    }
  }
  return event_vector;
}

ActionType Parser::recognizeAction(std::string readingValue){

  ActionType action;

  if (readingValue==CMD_TAKEOFF)
  {
    action = ActionType::takeOff;
  }
  else if(readingValue==CMD_HOVER)
  {
    action = ActionType::hover;
  }
  else if(readingValue==CMD_LAND)
  {
    action = ActionType::land;
  }
  else if(readingValue==CMD_MOVEMENT_POSITION)
  {
    action = ActionType::positionMovement;
  }
  else if(readingValue==CMD_GO_TO_POINT)
  {
    action = ActionType::goToPoint;
  }
  else if(readingValue==CMD_ROTATE_YAW)
  {
    action = ActionType::rotateYaw;
  }
  else if(readingValue==CMD_FLIP)
  {
    action = ActionType::flipMovement;
  }
  else if(readingValue==CMD_STABILIZE)
  {
    action = ActionType::stabilize;
  }
  else if(readingValue==CMD_REMEMBER_POINT)
  {
    action = ActionType::rememberPoint;
  }
  else
  {
    //Checking invalid action
    if(readingValue!="")
      parser_error_handler.errorFound("Unknown action \"" + readingValue + "\".");
    
  }
  return action;
}

SkillType Parser::recognizeSkill(std::string readingValue){

  SkillType task_skill;

  if(readingValue==BHV_AVOIDING_OBSTACLES)
  {
    task_skill=SkillType::avoidingObstacles;
  }
  else if(readingValue==BHV_RECOGNIZING_ARUCO_MARKERS)
  {
    task_skill=SkillType::recognizingArucoMarkers;
  }
  else if(readingValue==BHV_RECOGNIZING_VISUAL_MARKERS)
  {
    task_skill=SkillType::recognizingVisualMarkers;
  }
  else if(readingValue==BHV_SAYING_OUT_LOUD_CURRENT_TASK)
  {
    task_skill=SkillType::sayingOutLoudCurrentTask;
  }
  else
  {
    if(readingValue!="")
      parser_error_handler.errorFound("Unknown skill \"" + readingValue + "\".");
  }

  return task_skill;
}

EndingStepType Parser::recognizeEndingStep(std::string readingValue)
{
  EndingStepType ending_step;
  if (readingValue==END_NEXT_TASK)
  {
    ending_step = EndingStepType::nextTask;;
  }
  else if(readingValue==END_REPEAT_TASK)
  {
    ending_step = EndingStepType::repeatTask;
  }
  else if(readingValue==END_ABORT_MISSION)
  {
    ending_step = EndingStepType::abortMission;
  }
  else if(readingValue==END_END_MISSION)
  {
    ending_step = EndingStepType::endMission;
  }
  else
  {
   parser_error_handler.errorFound("Unknown termination mode \"" + readingValue + "\"."); 
  }
  return ending_step;
}

void Parser::recognizeParameter(std::string parameter)
{
  if(parameter != PAR_ARUCO_VISUAL_MARKER && parameter != PAR_BATTERY_CHARGE_PERCENTAGE && parameter != PAR_CURRENT_TASK)
    parser_error_handler.errorFound("Parameter \"" + parameter +"\" not found.");
}

void Parser::recognizeComparison(std::string comparison)
{
  if(comparison != COMP_EQUAL && comparison != COMP_NOT_EQUAL && comparison != COMP_LESS && 
    comparison != COMP_LESS_OR_EQUAL && comparison != COMP_GREATER && comparison != COMP_GREATER_OR_EQUAL 
    && comparison != COMP_INCLUDES)
    parser_error_handler.errorFound("Comparison \"" + comparison +"\" not found.");
}

std::vector<std::string> Parser::getErrors()
{
  return parser_error_handler.getErrors();
}

void Parser::displayFoundErrors()
{
  parser_error_handler.displayErrorsAndWarnings();
}
