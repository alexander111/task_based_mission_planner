#include <stdio.h>
#include <iostream>
#include "Task.h"

Task::Task()
{
  absolute_name   = "Default absolute name";
  description     = "Default description";
  //The task should be executed at least once.
  times_to_repeat = 1;
  action.setActionType(ActionType::empty);
}

Task::~Task()
{

}

int Task::setSkills(std::vector<SkillType> task_skills)
{
  skills = task_skills;
  return 0;
}
int Task::setDescription(std::string task_description)
{
  description = task_description;
  return 0;
}
int Task::setAction(Action action_to_set)
{
  action = action_to_set;
  return 0;
}
int Task::setAbsoluteName(std::string name)
{
  absolute_name = name;
  return 0;
}
int Task::setConditions(std::vector<Condition> task_conditions)
{
  conditions = task_conditions;
  return 0;
}
int Task::setTimesToRepeat(int n)
{
  times_to_repeat = n;
  return 0;
}

Action * Task::getAction()
{
  Action * action_to_return = &action; 
  return action_to_return;
}

std::string Task::getDescription()
{
  return description;
}
std::vector<SkillType> * Task::getSkills()
{
  std::vector<SkillType>* skill_vector = &skills;
  return skill_vector;
}
std::vector<Condition> * Task::getConditions()
{
  std::vector<Condition> * condition_vector = &conditions;
  return condition_vector;
}
std::string Task::getAbsoluteName()
{
  return absolute_name;
}
int Task::getTimesToRepeat()
{
  return times_to_repeat;
}
bool Task::checkTaskConditions(Parameter_Handler parameter_handler)
{
  //If no conditions are specified, then the task is executable.
  bool task_executable = true;
  for(uint i = 0; i < conditions.size() && task_executable; i++)
    task_executable = (task_executable && conditions.at(i).evaluateCondition(parameter_handler));
  return task_executable;
  
}
