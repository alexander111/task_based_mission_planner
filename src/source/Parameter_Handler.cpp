#include "Parameter_Handler.h"
using namespace std;

Parameter_Handler::Parameter_Handler()
{

}

Parameter_Handler::~Parameter_Handler()
{

}

void Parameter_Handler::updateTableOfParameters(std::pair<std::string, std::string> entry)
{
  bool found = false;
  for(uint i = 0; i < table_of_parameters.size() && !found; i++)
  {
    if(table_of_parameters.at(i).first == entry.first)
    {
      table_of_parameters.at(i).second = entry.second;
      found = true;
    }

  }
  if(!found)
    table_of_parameters.push_back(entry);
}
std::string Parameter_Handler::fromIntVectorToString(std::vector<int> vec)
{
  //The vector is saved according following syntaxis: [id0, id1,...]
  std::string value="[";
  //Generate value.
  for (int i = 0; i<vec.size(); i++)
  {
    if(i == vec.size()-1)
      value = value + fromNumberToString(vec.at(i));
    else
      value = value + fromNumberToString(vec.at(i))+", ";
  }
  value = value +"]";
  return value;
}

std::string Parameter_Handler::fromNumberToString(double n)
{
	//TO DO: Need to check if the given argument is a number.
	return to_string(n);
}

bool Parameter_Handler::existsInTableOfParameters(std::string par_name)
{
	bool found = false;
	for(uint i = 0; i<table_of_parameters.size() && !found; i++)
    found = (table_of_parameters.at(i).first == par_name);
	return found;
}
std::pair<std::string,std::string> Parameter_Handler::findInTableOfParameters(std::string par_name)
{
	//It is necessary to check if the element exists in the table of parameters.
	bool found   = false;
	std::pair<std::string,std::string> element;
  for(uint i=0; i< table_of_parameters.size(); i++)
  {
 	  found = (table_of_parameters.at(i).first == par_name);
    if(found)
      element = std::make_pair(par_name, table_of_parameters.at(i).second);
  }
  return element;
}