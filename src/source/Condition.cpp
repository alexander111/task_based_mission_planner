#include "Condition.h"
#include <boost/regex.hpp>

using namespace std;

Condition::Condition(std::string condition_parameter, std::string condition_comparison, std::string condition_value){
    //Default values
    parameter           = condition_parameter;
    comparison          = condition_comparison;
    value               = condition_value;
    condition_evaluation = false;
}

Condition::~Condition()
{
    
}

std::string Condition::getConditionExpresion()
{
    return parameter+" "+comparison+" "+value;
}

bool Condition::evaluateCondition(Parameter_Handler parameter_handler)
{
    // Buscar el parametro de la condición en la tabla, dpendiendo del parámetro tratarlo
    // y realizar la comparación. Si el parámetro no se encuentra es que no ha sido incializado
    // y en consecuencia la condición no se cumple.
  std::pair<std::string,std::string> entry;
  if(parameter_handler.existsInTableOfParameters(parameter))
  {
    entry = parameter_handler.findInTableOfParameters(parameter);
    std::string condition_parameter = entry.second;
    if(parameter == PAR_BATTERY_CHARGE_PERCENTAGE)
    {
      if (comparison == COMP_EQUAL)
        condition_evaluation = (stod(condition_parameter) == stod(value));
      else if(comparison == COMP_NOT_EQUAL)
        condition_evaluation = (stod(condition_parameter) != stod(value));
      else if(comparison == COMP_LESS)
        condition_evaluation = (stod(condition_parameter) < stod(value));
      else if(comparison == COMP_LESS_OR_EQUAL)
        condition_evaluation = (stod(condition_parameter) <= stod(value));
      else if(comparison == COMP_GREATER)
        condition_evaluation = (stod(condition_parameter) > stod(value));
      else if(comparison == COMP_GREATER_OR_EQUAL)
        condition_evaluation = (stod(condition_parameter) >= stod(value));
      //This case should never be reached.
      else
        cout << "Unsupported comparison symbol!" << endl;
    }
    else if(parameter == PAR_CURRENT_TASK)
    {
      if (comparison == COMP_EQUAL)
        condition_evaluation = (condition_parameter == value);
      else if(comparison == COMP_NOT_EQUAL)
        condition_evaluation = (condition_parameter != value);
      else if(comparison == COMP_LESS)
        condition_evaluation = (condition_parameter < value);
      else if(comparison == COMP_LESS_OR_EQUAL)
        condition_evaluation = (condition_parameter <= value);
      else if(comparison == COMP_GREATER)
        condition_evaluation = (condition_parameter > value);
      else if(comparison == COMP_GREATER_OR_EQUAL)
        condition_evaluation = (condition_parameter >= value);
      //This case should never be reached.
      else
        cout << "Unsupported comparison symbol!" << endl;
    }
    else if(parameter == PAR_ARUCO_VISUAL_MARKER)
    {
      std::vector<int> int_vector = fromStringtoIntVector(condition_parameter);
      // It is not necessary to check if the vector is empty since we only enter in this
      // case when a visual marker has been identified, so the vector must have at least
      // one element.
      if (comparison == COMP_INCLUDES && is_number(value))  
        condition_evaluation = belongsInt(stoi(value), int_vector);
      else 
        condition_evaluation = false; 
    }
  }
  return condition_evaluation; 
}

std::vector<int> Condition::fromStringtoIntVector(std::string vector)
{
  //The pointer points to the first character of the string.
  
  std::vector<int> int_vector;
  int aux;
  for(char& c : vector){
    if(isdigit(c))
    {
      aux= c-'0';
      int_vector.push_back(aux);
    }
  }
  return int_vector;
}

bool Condition::isParameterEqualTo(std::string condition_parameter)
{
	return condition_parameter == parameter;
}

bool Condition::getConditionValue()
{
    return condition_evaluation;
}

void Condition::resetConditionEvaluation()
{
 condition_evaluation = false;
}

bool Condition::belongsInt( int n, std::vector<int> vec)
{
  bool found = false;
  uint i      = 0;
  while(!found && i<vec.size())
  {
    found = (n == vec.at(i));
    i++;
  }
  return found;
}

bool Condition::is_number(const std::string& s)
{
  return !s.empty() && std::find_if(s.begin(), 
      s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}
