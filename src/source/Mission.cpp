#include <stdio.h>
#include <iostream>
#include "Mission.h"

using namespace std;

Mission::Mission(TreeNode<Task> generatedTaskTree, Parameter_Handler &par_handler){
  //Obtaining generated task tree from parsing.
  taskTree = generatedTaskTree;
  name = taskTree.getNode()->getDescription();
  parameter_handler = &par_handler;
}
Mission::~Mission()
{

}

bool Mission::missionCompleted()
{
  return taskTree.getChildren()->empty();
}

std::vector<SkillType> Mission::getActiveSkills()
{
  return getActiveSkillsAux(taskTree);
}

std::vector<SkillType> Mission::getActiveSkillsAux(TreeNode<Task> tree)
{ 
  //Variable declaration
  std::vector<SkillType> toReturn;
  std::vector<SkillType> auxVector;
  std::vector<SkillType> activeSkills = *(tree.getNode()->getSkills());
 
  if(!tree.getChildren()->empty())
    auxVector = getActiveSkillsAux(tree.getChildren()->at(0));

  //Preallocate memory
  toReturn.reserve( activeSkills.size() + auxVector.size() ); 
  //Concatenate  
  toReturn.insert( toReturn.end(), activeSkills.begin(), activeSkills.end() );
  toReturn.insert( toReturn.end(), auxVector.begin(), auxVector.end() );
  
  return toReturn;
}

Task Mission::nextTask()
{
  return nextTaskAux(taskTree);
}

Task Mission::nextTaskAux(TreeNode<Task> &tree)
{
  std::vector <TreeNode<Task> > *child = tree.getChildren();
  Task taskToReturn;
  for(std::vector<TreeNode<Task> >::iterator iter = child->begin(); !child->empty() && iter != child->end(); ++iter)
  {
    //Updating current task...
    parameter_handler->updateTableOfParameters(std::make_pair(PAR_CURRENT_TASK, iter->getNode()->getAbsoluteName()));
    //Delete those task which conditions are not true and update the current task.
    while(!iter->getNode()->checkTaskConditions(*parameter_handler) && iter != child->end())
    {
      iter = child->erase(iter);
      parameter_handler->updateTableOfParameters(std::make_pair(PAR_CURRENT_TASK, iter->getNode()->getAbsoluteName()));
    }
    if(iter->getChildren()->empty() && !iter->getNode()->getAction()->isEmpty() && iter->getNode()->checkTaskConditions(*parameter_handler))
    {
      taskToReturn = *(iter->getNode());
      iter = child->erase(iter);
    }
    else if(!iter->getChildren()->empty() && iter->getNode()->checkTaskConditions(*parameter_handler)) 
      taskToReturn = Mission::nextTaskAux(*iter);
    if(iter->getChildren()->empty() && iter->getNode()->getAction()->isEmpty()) 
      child->erase(iter);
    //Finaly, update the parameter current task.
    parameter_handler->updateTableOfParameters(std::make_pair(PAR_CURRENT_TASK, taskToReturn.getAbsoluteName()));
    return taskToReturn;    
  }
}


int Mission::printTree(){
  std::string message = "";//Message to display.
  std::string acc     = "";//Just to make the print readable, it is a tab accumulator.
  cout << "--------------------TASK TREE--------------------" << endl;
  cout << Mission::printTreeAux(taskTree, message, acc); 
  return 0;
}

std::string Mission::printTreeAux(TreeNode<Task> taskTree, std::string message, std::string acc){
  Task *current_task = taskTree.getNode();
  std::vector <TreeNode<Task> > *child= taskTree.getChildren();
  std::vector <SkillType> *skills = current_task->getSkills();
  std::vector <Condition> *conditions = current_task->getConditions();

  //Printing task attributes
  message = message +  acc + "\033[1;34mTask: " + current_task->getDescription() + "\033[0m\n";

  if(!conditions->empty()){
    for(uint j =0; j < conditions->size(); j++) 
      message = message + acc +"\t\033[1;33mCondition: \033[0m" + conditions-> at(j).getConditionExpresion() + "\n"; 
  }   
  if(!skills->empty()){
    for(uint j=0; j< skills->size(); j++)
      if(skills->at(j) == SkillType::avoidingObstacles)
        message = message + acc + "\t Skill: " + BHV_AVOIDING_OBSTACLES + "\n";
      else if(skills->at(j) == SkillType::recognizingArucoMarkers)
        message = message + acc + "\t Skill: " + BHV_RECOGNIZING_ARUCO_MARKERS + "\n";
      else if(skills->at(j) == SkillType::recognizingVisualMarkers)
        message = message + acc + "\t Skill: " + BHV_RECOGNIZING_VISUAL_MARKERS + "\n";
      else if(skills->at(j) == SkillType::sayingOutLoudCurrentTask)
        message = message + acc + "\t Skill: " + BHV_SAYING_OUT_LOUD_CURRENT_TASK + "\n";
      else
        message = message + acc + "\t Skill: " + "Unknown skill!!" + "\n";
  }
  if(!current_task->getAction()->isEmpty())
    message = message+ acc + "\t Action: " + current_task->getAction()->getReadableName() + "\n";

  //For every child of the current task, repeat the action.
  acc = acc + "\t";
  if(!child -> empty()){
    for(uint i=0; i < child->size(); i++){ 
      message = Mission::printTreeAux(child->at(i), message, acc); 
    }
  }
  return message;         
 }

 std::string Mission::getName()
 {
   return name;
 }

