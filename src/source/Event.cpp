#include <stdio.h>
#include <iostream>
#include "Event.h"

using namespace std;

Event::Event(std::string eventDescription, std::vector<Condition> eventConditions, std::vector<Action> eventActions, EndingStepType event_ending_step){
    //Intialize attributes.
    description       = eventDescription;
    conditions        = eventConditions;
    actions           = eventActions;
    remaining_actions = eventActions;
    ending_step       = event_ending_step;
}

Event::~Event()
{

}
bool Event::isEventActivated(Parameter_Handler par)
{
    bool activated = true;
    for(uint i=0; i<conditions.size(); i++)
       activated = activated && conditions.at(i).evaluateCondition(par);
    return activated;
}
std::string Event::getDescription(){
    return description;
}
std::vector<Action> * Event::getActions(){
    std::vector<Action> * actions_vector = &actions;
    return actions_vector;
}

std::vector<Action> * Event::getRemainingActions(){
    std::vector<Action> * actions_vector = &remaining_actions;
    return actions_vector;
}

EndingStepType Event::getEndingStep(){
    return ending_step;
}

std::vector<Condition>* Event::getConditions(){
    std::vector<Condition>* conditions_vector = &conditions;
    return conditions_vector;
}
void Event::resetEvent()
{
    remaining_actions = actions;
    for (uint i =0; i<conditions.size(); i++)
      conditions.at(i).resetConditionEvaluation();
}
